package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {
    int rows;
    int columns;
    cellular.CellState[][] CellState;


    public CellGrid(int rows, int columns, CellState initialState) {
		this.rows = rows;
        this.columns = columns;
        this.CellState = new CellState[rows][columns];
	}

    @Override
    public int numRows() {
        // TODO Auto-generated method stub
        return this.rows;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return this.columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        // TODO Auto-generated method stub
        this.CellState[row][column] = element;
        
    }

    @Override
    public CellState get(int row, int column) {
        // TODO Auto-generated method stub
        return this.CellState[row][column];
    }

    @Override
    public IGrid copy() {
        // TODO Auto-generated method stub
        CellGrid kopi = new CellGrid(this.rows, this.columns, null);
        for (int row = 0; row < this.rows; row++){
            for (int column = 0; column < this.columns; column++){
                kopi.set(row, column, this.get(row, column));
            }
        }
        return kopi;
    }
    
}
